//
//  TableViewController.swift
//  BasicUITableView
//
//  Created by ivanko on 26/10/2020.
//  Copyright © 2020 ivk. All rights reserved.
//

import UIKit

class makeActions{
    let titleAction: String
    var isGoodAction: Bool
    
    init( title: String,
          isGoodAction: Bool){
        
        titleAction = title
        self.isGoodAction = isGoodAction
    }
    
}

class TableViewController: UITableViewController{
    
    var models: [(String, () -> ())] = []
    var actionsGoodOrBad = [makeActions]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for _ in 1...10 {
//            let random = Int.random(in: 0...1)
//            if random == 1{
//                actionsGoodOrBad.append (makeActions.init(title: "is this a Good Action", isGoodAction: true))
//            }else{
//                actionsGoodOrBad.append (makeActions.init(title: "This Action not a Good Action", isGoodAction: false))
//            }
            actionsGoodOrBad.append (makeActions.init(title: "is this a Good Action", isGoodAction: true))
        }
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "BasicCell")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        myLogs.traceInstance.printTrace (text: "viewDidAppear")
    }
    
    // tableView data processing! ********************************************************************
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return actionsGoodOrBad.count / 2
        }else{
            return actionsGoodOrBad.count / 2
        }
        
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       // Fetch a cell of the appropriate type.
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell", for: indexPath)
       // Configure the cell’s contents.
        cell.textLabel!.text = actionsGoodOrBad[indexPath.row].titleAction
           
       return cell
    }
override func tableView(_ tableView: UITableView,
                        titleForHeaderInSection section: Int) -> String? {
    if (section == 0){
        return "myHeader section 0!"
        
    }else{
        return "myHeader section 1!"
    }
}
override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
    if (section == 0){
        return "myFooter section 0!"
        
    }else{
        return "myFooter section 1!"
    }
}
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        2
    }
    
    // tableView Delegate processing! ********************************************************************
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if (indexPath.row == 1) {
//            print ("this is the two")
//            cell.backgroundColor = .red
//        }
    }
    override func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        print ("didEndDisplaying this cell:")
        print ("    cell: \(cell)")
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //tableView.deselectRow(at: indexPath, animated: true)
        //models[indexPath.row].1()
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // let vForHeader = oneLabelWithTextPlease( text: "This is the header")
        let vForHeader = UIView();
        vForHeader.backgroundColor = .cyan
        return vForHeader
    }

    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vFooter = oneLabelWithTextPlease(text: "This is the footer!")
        vFooter.backgroundColor = .systemPink
        return vFooter
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (section == 0){
            return 125
            
        }else{
            return 75
        }
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        50
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if (section == 0){
            return 125
            
        }else{
            return 75
        }
    }

    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let aGoodOne = goodAction(at: indexPath)
        let aNotGoodOne = noGoodAction(at: indexPath)
        return UISwipeActionsConfiguration(actions: [aGoodOne, aNotGoodOne])
    }
    
    func goodAction(at IndexPath: IndexPath) -> UIContextualAction {
        let actionRow = actionsGoodOrBad[IndexPath.row]
        let action = UIContextualAction(style: .normal, title: "A Good One"){
            (action, view, completion) in
            actionRow.isGoodAction = true
            completion(true)
        }
        action.image = .add
        action.backgroundColor = .green
        return action
    }

    func noGoodAction(at IndexPath: IndexPath) -> UIContextualAction {
        // let actionRow = actionsGoodOrBad[IndexPath.row]
        let action = UIContextualAction(style: .destructive, title: "Remove"){
            (action, view, completion) in
            self.actionsGoodOrBad.remove(at: IndexPath.row)
            self.tableView.deleteRows(at: [IndexPath], with: .automatic)
            completion(true)
        }
        action.image = .remove
        action.backgroundColor = .red
        return action
    }

}


// Out of protocols.
extension TableViewController {
    func addLabelToView (v: UIView, text: String){
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 40))
        label.text = text
        v.addSubview(label)
    }
    
    func oneLabelWithTextPlease (text: String) -> UILabel{
        let label = UILabel(frame: CGRect(x: 70, y: 0, width: 200, height: 200))
        label.text = text
        //v.addSubview(label)
        return label
    }
}
