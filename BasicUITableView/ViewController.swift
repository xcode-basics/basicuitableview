//
//  ViewController.swift
//  BasicUITableView
//
//  Created by ivanko on 26/10/2020.
//  Copyright © 2020 ivk. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // Templates for the data!
    var myDataModel: [(String, String)]! // Static in application.
    var myDownloadedDataModel: [(String, String)]! // Downloaded from any host.
    var nextLastIndex = 0
    var bUpDate: Bool = false
    
    @IBOutlet weak var labelmyDataModelCount: UILabel!
    @IBOutlet weak var labelNumberOfRowsInSection: UILabel!
    
    @IBOutlet weak var allowsSelection: UISwitch!
    @IBOutlet weak var allowsMultipleSelection: UISwitch!
    
    // Outlet connection from/to storyboard!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Delegation for events receptions!
        tableView.delegate = self
        // Delegation for Data management!
        tableView.dataSource = self
        
        tableView.prefetchDataSource = self
        tableView.allowsSelection = true
        tableView.allowsMultipleSelection = false
        // Our Data Model initialization!
        let initialDataModel = [
                        ("00 ","initialDataModel"), ("01 ","initialDataModel"),
                        ("02 ","initialDataModel"), ("03 ","initialDataModel"),
                        ("04 ","initialDataModel"), ("05 ","initialDataModel"),
                        ("06 ","initialDataModel"), ("07 ","initialDataModel"),
                    ]
        myDataModel = initialDataModel
        nextLastIndex = myDataModel.count;
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: selectedIndexPath, animated: animated)
        }
    }
    
    @IBAction func allowSelectionAction(_ sender: Any) {
        tableView.allowsSelection = allowsSelection.isOn
        tableView.reloadData()
    }
    
    
    @IBAction func updateReady(){
        myLogs.traceInstance.printTrace (text: "adding Data to the table")
        self.addData()
        self.tableView.reloadData()
        self.bUpDate = false
    }
    
    @IBAction func buttonTableViewController(_ sender: Any) {
        
        let tvc = TableViewController()
        tvc.models = [ ("first",    { myLogs.traceInstance.printTrace (text: "something") }),
                       ("2nd",      { myLogs.traceInstance.printTrace (text: "something") }),
                       ("3th",      { myLogs.traceInstance.printTrace (text: "something") }),
        ]
        
        present(tvc, animated: true, completion: nil)
    }
    
}

// MARK
// UITableViewDelegate protocol to manage TableView events.
extension ViewController: UITableViewDelegate {

    // Configuring Rows for the Table View
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        myLogs.traceInstance.printTrace(text: "\nStarts (tableView willDisplay cell) - Tells the delegate the table view is about to draw a cell for a particual row")
        myLogs.traceInstance.printTrace(text: "      willDisplay cell = \(cell)")
        myLogs.traceInstance.printTrace(text: "      forRowAt indexPath is = \(indexPath)")
        
    }
    
    func tableView(_ tableView: UITableView, indentationLevelForRowAt indexPath: IndexPath) -> Int {
        myLogs.traceInstance.printTrace(text: "\nStarts (tableView indentationLevelForRowAt indexPath) - Asks the delegate to return the level of indentation for a row in a given section")
        myLogs.traceInstance.printTrace(text: "      indentationLevelForRowAt indexPath is = \(indexPath)")
        return 200
    }
    
    func tableView(_ tableView: UITableView, shouldSpringLoadRowAt indexPath: IndexPath, with context: UISpringLoadedInteractionContext) -> Bool {
        myLogs.traceInstance.printTrace(text: "\nStarts (tableView indentationLevelForRowAt indexPath) - Called to let you fine tune the spring-loading behavior of the rows in a table. ")
        myLogs.traceInstance.printTrace(text: "      shouldSpringLoadRowAt indexPath is = \(indexPath)")
        
        return false
    }
    
    
    // Responding to Row Selections
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        myLogs.traceInstance.printTrace(text: "\nStarts (tableView willSelectRowAt indexPath) -Tells the delegate a row is about to be selected.")
        
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        buttonTableViewController( indexPath)
    }

    func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        myLogs.traceInstance.printTrace(text: "\nStarts (tableView willSelectRowAt indexPath) -Tells the delegate a row is about to be deselected.")
        
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        myLogs.traceInstance.printTrace(text: "\nStarts (tableView willSelectRowAt indexPath) -Tells the delegate a row is about to be deselected.")
    }
    
    
    func tableView(_ tableView: UITableView, shouldBeginMultipleSelectionInteractionAt indexPath: IndexPath) -> Bool {
        myLogs.traceInstance.printTrace(text: "\nStarts (tableView willSelectRowAt indexPath) - Asks the delegate whether the user can use a two-finger pan gesture to select multiple items in a table view.")
        
        return false
    }
    
    func tableView(_ tableView: UITableView, didBeginMultipleSelectionInteractionAt indexPath: IndexPath) {
        myLogs.traceInstance.printTrace(text: "\nStarts (tableView(_ tableView: UITableView, didBeginMultipleSelectionInteractionAt indexPath: IndexPath) - Tells the delegate when the user starts using a two-finger pan gesture to select multiple rows in a table view.")
        
    }
    
    func tableViewDidEndMultipleSelectionInteraction(_ tableView: UITableView) {
        myLogs.traceInstance.printTrace(text: "\nStarts (tableViewDidEndMultipleSelectionInteraction(_ tableView: UITableView)) - Tells the delegate when the user stops using a two-finger pan gesture to select multiple rows in a table view.")
        
    }

    
    // Providing Custom Header and Footer Views
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        myLogs.traceInstance.printTrace(text: "\nStartstableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?) - Asks the delegate for a view object to display in the header of the specified section of the table view.")
        return nil
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        myLogs.traceInstance.printTrace(text: "\nStartstableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?) - Asks the delegate for a view object to display in the footer of the specified section of the table view.")
        return nil
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
       myLogs.traceInstance.printTrace(text: "\nStartstableView(tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)) - Tells the delegate that the table is about to display the header view for the specified section")
    }
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        myLogs.traceInstance.printTrace(text: "\nStartstableView(tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int)) - Tells the delegate that the table is about to display the footer view for the specified section")
    }
    
// Providing Header, Footer, and Row Heights
    
    
}

// UITableViewDataSource protocol to inject the data into the table.
extension ViewController: UITableViewDataSource {
    
    // UITableViewDataSource - mandatory methods to be implemented.
    // UITableViewDataSource numberOfRowsInSection - reports about the total rows. Only executed once!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let totalRows = myDataModel.count + 1
        myLogs.traceInstance.printTrace (text: "numberOfRowsInSection - totalRows = \(totalRows)")
        labelNumberOfRowsInSection.text = "\(totalRows)"
        labelmyDataModelCount.text = "\(myDataModel.count)"
        return totalRows
    }
    
    // UITableViewDataSource cellForRowAt - it configures the cell row . It is executed as many times as numberOfRowsInSection returned!.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        if self.bUpDate == true {
            self.updateReady()
        }
        // Check if last row is visible.
        if indexPath.row  == self.myDataModel.count {
            myLogs.traceInstance.printTrace (text: "numberOfRowsInSection - cellForRowAt - Data on cell: DownLoading...")
            cell = tableView.dequeueReusableCell(withIdentifier: "basicCell", for: indexPath)
            cell.textLabel?.text = "Loading..."// "IndexPath = \(indexPath): \(myDataModel[indexPath.row].0) or \(myDataModel[indexPath.row].1)"
        }else{
            
            let cellLabel = "IndexPath = \(indexPath): \(myDataModel[indexPath.row].0) !! \(myDataModel[indexPath.row].1)"
            
            cell = tableView.dequeueReusableCell(withIdentifier: "basicCell", for: indexPath)
            cell.textLabel?.text = cellLabel
            
            myLogs.traceInstance.printTrace (text: "numberOfRowsInSection - cellForRowAt - Data on cell: \(cellLabel)")
        }
        return cell
    }
}

extension ViewController:UITableViewDataSourcePrefetching{
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        let visibleCells = tableView.visibleCells
        for cell in visibleCells{
            if (cell.textLabel?.text == "Loading..."){
                if (bUpDate == true){
                    myLogs.traceInstance.printTrace (text: "\n****** yeeaaaa! \(myDataModel.count)\n")
                    updateReady()
                }

            }
        }
        
        myLogs.traceInstance.printTrace (text: "\n****** prefechtData myDataModel.count \(myDataModel.count)\n")
        for indexPath in indexPaths{
            
            if (0 != indexPath.row){
                if (indexPath.row == myDataModel.count){
                    if (nextLastIndex == myDataModel.count){
                        prefechtData(ofIndex: indexPath.row)
                    }
                }
            }
        }
    }
    
    func addData() {
        myDataModel = myDataModel + myDownloadedDataModel
        //self.labelmyDataModelCount.text = "\(myDataModel.count)"
    }
    
    func prefechtData (ofIndex index: Int){
        
        let queue = DispatchQueue(label: "goFakeInterneT")
        myLogs.traceInstance.printTrace (text: "queue.async - Going to fake interneT!")
        queue.async {

            self.myDownloadedDataModel = [
                                            ("\(index)","prefechData"),
                                            ("\(index + 1)","prefechData"),
                                            ("\(index + 2)","prefechData")
                                         ]

            self.nextLastIndex = self.myDataModel.count + self.myDownloadedDataModel.count
            sleep(5)
            myLogs.traceInstance.printTrace (text: "queue.async - Received Data from fake interneT! \(self.myDownloadedDataModel!)")

            // Notify to main thread, tableview cellForRowAt indexPath method, that data from fake interneT is ready!
            self.bUpDate = true
            DispatchQueue.main.async{
                self.updateReady()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cancelPrefetchingForRowsAt indexPaths: [IndexPath]) {
            myLogs.traceInstance.printTrace(text: "cancelPrefetchingForRowsAt")
    }
}

// Out of any delegation!
class myLogs {
    
    public static var traceInstance = myLogs()
    var areTracesOn: Bool = true
    
    func printTrace( text: String){
        if (myLogs.traceInstance.areTracesOn == true){
            print(text)
        }
    }
    
    
}
